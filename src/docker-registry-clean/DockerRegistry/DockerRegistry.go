package DockerRegistry

import (
	"net/http"
	"strings"

	"encoding/json"
	"io/ioutil"

	"fmt"

	log "github.com/cihub/seelog"
)

type DockerRegistryService struct {
	url      string
	userName string
	password string
}

type _DockerRegistryRepositoryListResponse struct {
	Repositories []string `json:"repositories"`
}

type _DockerRegistryRepositoryTagListResponse struct {
	Name string   `json:"name"`
	Tags []string `json:"tags"`
}

func (this *DockerRegistryService) addRequestAuthentication(req *http.Request) {
	req.SetBasicAuth(this.userName, this.password)
}

func (this *DockerRegistryService) getData(resourcePath string, resultData interface{}) error {
	req, err := http.NewRequest("GET", this.url+resourcePath, nil)
	if err != nil {
		return log.Error(err)
	}
	this.addRequestAuthentication(req)
	log.Infof("call %s %s", req.Method, req.URL.String())
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return log.Error(err)
	}
	if resp.Body != nil {
		defer resp.Body.Close()
	}
	if resp.StatusCode != http.StatusOK {
		return log.Errorf("docker registry reponse err code: %d", resp.StatusCode)
	}
	jsonData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return log.Error(err)
	}
	if err := json.Unmarshal(jsonData, resultData); err != nil {
		return log.Error(err)
	}
	return nil
}

func (this *DockerRegistryService) getRepositoryTaggedManifestDigest(repository, tag string) (string, error) {
	req, err := http.NewRequest("HEAD", this.url+fmt.Sprintf("v2/%s/manifests/%s", repository, tag), nil)
	if err != nil {
		return "", log.Error(err)
	}
	this.addRequestAuthentication(req)
	log.Infof("call %s %s", req.Method, req.URL.String())
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", log.Error(err)
	}
	if resp.Body != nil {
		defer resp.Body.Close()
	}
	if resp.StatusCode != http.StatusOK {
		return "", log.Errorf("docker registry reponse err code: %d", resp.StatusCode)
	}
	return strings.TrimSpace(resp.Header.Get("Docker-Content-Digest")), nil
}

func (this *DockerRegistryService) GetRepositoryNames() ([]string, error) {
	var response _DockerRegistryRepositoryListResponse
	if err := this.getData("v2/_catalog", &response); err != nil {
		return nil, err
	}
	return response.Repositories, nil
}

func (this *DockerRegistryService) GetRepositoryTaggedManifestDigests(repository string) (map[string]string, error) {
	var tags _DockerRegistryRepositoryTagListResponse
	if err := this.getData(fmt.Sprintf("v2/%s/tags/list", repository), &tags); err != nil {
		return nil, err
	}
	result := make(map[string]string, len(tags.Tags))
	for _, tag := range tags.Tags {
		if digest, err := this.getRepositoryTaggedManifestDigest(repository, tag); err == nil {
			result[tag] = digest
		} else {
			return nil, err
		}
	}
	return result, nil
}

func (this *DockerRegistryService) DeleteRepositoryTaggedManifest(repository, digest string) error {
	req, err := http.NewRequest("DELETE", this.url+fmt.Sprintf("v2/%s/manifests/%s", repository, digest), nil)
	if err != nil {
		return log.Error(err)
	}
	this.addRequestAuthentication(req)
	log.Infof("call %s %s", req.Method, req.URL.String())
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return log.Error(err)
	}
	if resp.Body != nil {
		defer resp.Body.Close()
	}
	if resp.StatusCode != http.StatusAccepted {
		return log.Errorf("docker registry reponse err code: %d", resp.StatusCode)
	}
	return nil
}

func NewDockerRegistryService(url, username, password string) *DockerRegistryService {
	if !strings.HasSuffix(url, "/") {
		url += "/"
	}
	return &DockerRegistryService{
		url:      url,
		userName: username,
		password: password,
	}
}
