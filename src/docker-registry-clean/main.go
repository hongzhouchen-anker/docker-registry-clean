package main

import (
	"docker-registry-clean/DockerManifestListParser"
	"docker-registry-clean/DockerRegistry"

	"os"

	"fmt"

	log "github.com/cihub/seelog"
	"github.com/mkideal/cli"
)

type Argument struct {
	cli.Helper
	RegistryUrl      string `cli:"registryurl" usage:"docker registry url" dft:"https://ycrm-cn-ci.ankerjiedian.com:5000/"`
	LoginUserName    string `cli:"*loginusername" usage:"login user name"`
	LoginPassword    string `cli:"*loginpassword" usage:"login password"`
	ManifestListPath string `cli:"*manifestlistpath" usage:"manufest list from docker registry garbage collection dry-run output"`
}

func main() {
	defer log.Flush()
	root := &cli.Command{
		Name: "docker-registry-clean",
		Argv: func() interface{} { return new(Argument) },
		Fn: func(ctx *cli.Context) error {
			argv := ctx.Argv().(*Argument)
			return deleteUnusedManifest(argv)
		},
	}
	if err := cli.Root(root).Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func deleteUnusedManifest(argv *Argument) error {
	registryService := DockerRegistry.NewDockerRegistryService(argv.RegistryUrl,
		argv.LoginUserName, argv.LoginPassword)
	manufestsInUsing, err := getManufestInUsing(registryService)
	if err != nil {
		return err
	}
	manifestParser, err := DockerManifestListParser.NewDockerManifestListParser(argv.ManifestListPath)
	if err != nil {
		return err
	}
	for manifestParser.HasNext() {
		reposiotry, digest := manifestParser.Next()
		if digests, found := manufestsInUsing[reposiotry]; found {
			isInWhiteList := false
			for _, item := range digests {
				if item == digest {
					isInWhiteList = true
					break
				}
			}
			if !isInWhiteList {
				log.Infof("Remove %s %s", reposiotry, digest)
				if err := registryService.DeleteRepositoryTaggedManifest(reposiotry, digest); err != nil {
					log.Errorf("Failed remove %s %s", reposiotry, digest)
				}
			}
		} else {
			log.Infof("Remove not in repository list manifest %s %s", reposiotry, digest)
			if err := registryService.DeleteRepositoryTaggedManifest(reposiotry, digest); err != nil {
				log.Errorf("Failed remove %s %s", reposiotry, digest)
			}
		}
	}
	return nil
}

func getManufestInUsing(registryService *DockerRegistry.DockerRegistryService) (map[string][]string, error) {
	repositoryList, err := registryService.GetRepositoryNames()
	if err != nil {
		return nil, err
	}
	manufestsInUsing := map[string][]string{}
	for _, repository := range repositoryList {
		if tagDigests, err := registryService.GetRepositoryTaggedManifestDigests(repository); err == nil {
			for _, digest := range tagDigests {
				manufestsInUsing[repository] = append(manufestsInUsing[repository], digest)
			}
		}
	}
	return manufestsInUsing, nil
}
