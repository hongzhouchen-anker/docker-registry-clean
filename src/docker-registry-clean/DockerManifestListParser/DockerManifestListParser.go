package DockerManifestListParser

import (
	"bufio"
	"os"

	"io"

	"strings"

	log "github.com/cihub/seelog"
)

type DockerManifestListParser struct {
	closer       io.Closer
	scaner       *bufio.Scanner
	nextManufest string
	nextDigest   string
}

func (this *DockerManifestListParser) parseNext() {
	if this.scaner != nil {
		for this.scaner.Scan() {
			line := this.scaner.Text()
			if strings.Contains(line, "marking manifest") {
				repositoryIndex := strings.Index(line, ":")
				digestIndex := strings.LastIndex(line, "sha256:")
				if repositoryIndex != -1 && digestIndex != -1 &&
					repositoryIndex < digestIndex {
					this.nextManufest = strings.TrimSpace(line[:repositoryIndex])
					this.nextDigest = strings.TrimSpace(line[digestIndex:])
					return
				}
			}
		}
		if this.scaner.Err() != nil {
			log.Error(this.scaner.Err())
		}
		this.scaner = nil
		this.closer.Close()
		this.closer = nil
		this.nextDigest = ""
		this.nextManufest = ""
	}
}

func (this *DockerManifestListParser) HasNext() bool {
	return this.nextManufest != "" && this.nextDigest != ""
}

func (this *DockerManifestListParser) Next() (string, string) {
	manufest, digest := this.nextManufest, this.nextDigest
	this.parseNext()
	return manufest, digest
}

func NewDockerManifestListParser(manifestListPath string) (*DockerManifestListParser, error) {
	var scaner *bufio.Scanner
	var closer io.Closer
	if _, err := os.Stat(manifestListPath); err == nil {
		file, err := os.Open(manifestListPath)
		if err != nil {
			return nil, log.Error(err)
		}
		scaner = bufio.NewScanner(file)
		closer = file
	} else {
		return nil, log.Errorf("not found %s: %s", manifestListPath, err)
	}
	parser := &DockerManifestListParser{
		scaner: scaner,
		closer: closer,
	}
	parser.parseNext()
	return parser, nil
}
