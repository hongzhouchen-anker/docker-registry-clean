package DockerManifestListParser

import (
	"testing"

	log "github.com/cihub/seelog"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDockerManifestListParser(t *testing.T) {
	defer log.Flush()
	Convey("TestDockerManifestListParser", t, func() {
		parser, err := NewDockerManifestListParser("./docker-manifest-list")
		So(err, ShouldBeNil)
		var results [][]string
		for parser.HasNext() {
			repository, digest := parser.Next()
			results = append(results, []string{repository, digest})
		}

		expectedResults := [][]string{
			[]string{"activiti-extension-base", "sha256:8b24f70d95ca996c7432440b256cef531262f16cced3cdc1bfc909c20be85165"},
			[]string{"dev_cn_activiti-extension", "sha256:27e3030e1c6379e98ab6db0809201b9a5ed2c8475576e4fe875f4d0da2e0ecac"},
			[]string{"dev_cn_activiti-extension", "sha256:8b55b0a2dd7281bcb5ccdfed41fc81df5870c28871365207339bdccc6733e9a4"},
			[]string{"dev_cn_activiti-extension", "sha256:b467320483cd1ddbd400abfd554b96bab7dcb8c38d9e1fd023d493446732a4fe"},
			[]string{"dev_cn_api", "sha256:55af763f3b4a51fd2c66779df3e09b291127cc70bf472d14eaa870ed6996164d"},
			[]string{"dev_cn_api", "sha256:67975ddd461415e7b46f9d8f87666908b802557396682bb18c8fa557abca0187"},
			[]string{"dev_cn_api", "sha256:7656e24b52355c232015120c557550696ad517a5daa839fa6b887bbd3df3ea03"},
			[]string{"dev_cn_userlib", "sha256:36780aa82b20956679e8ac05a53bc6b262fd74b7dabeab8077fdf86fc70c8f3c"},
			[]string{"dev_cn_userlib", "sha256:429c6deaf2edd923515712e5ad42959cf4b25df20365e47b77d2853f765bda0c"},
			[]string{"dev_cn_userlib", "sha256:e6cc162c33a6a646be0db2653b85b862de6cabcb545a3faba3f5033458f83068"},
			[]string{"dev_cn_web", "sha256:152aee0e310fd272e54fbc0f50286e56d16b1e89de8ad50bb2554ae6735c3708"},
			[]string{"dev_cn_web", "sha256:b6d88f09238f83d5871ad8d58c712a4ef7e551f242264543976f219326f270af"},
			[]string{"dev_cn_web", "sha256:d8fa057474cb63ad161cb5a40ef35bacea4f5a711f7f51c4a78fc860e771b9fd"},
		}
		So(len(results), ShouldEqual, len(expectedResults))
		for i := 0; i < len(expectedResults); i++ {
			So(results[i][0], ShouldEqual, results[i][0])
			So(results[i][1], ShouldEqual, results[i][1])
		}
	})
}
